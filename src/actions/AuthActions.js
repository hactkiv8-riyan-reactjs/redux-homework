export function Login(email, password) {
    return {
        type: 'LOGIN',
        email,
        password
    }
}