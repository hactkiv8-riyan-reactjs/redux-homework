const staticLogin = (inputEmail, inputPassword) => {

    let storedUsers = [
        { email: 'admin@hacktiv8.com', password: 'admin' },
        { email: 'riyan@hacktiv8.com', password: 'riyan' },
    ];

    let isValid = storedUsers.filter(({ email, password }) => email === inputEmail && password === inputPassword).length > 0;

    return (isValid) ? {
        loginIsValid: true,
        loginTitle: 'Login Success!',
        loginMessage: 'Your Login Credential is Valid!'
    } : {
            loginIsValid: false,
            loginTitle: 'Login Failed!',
            loginMessage: 'Login was failed, wrong email or password!'
        };

}

const AuthReducer = (state = [], action) => {
    switch (action.type) {
        case 'LOGIN': {
            return staticLogin(action.email, action.password);
        }
        default: {
            return state;
        }
    }
}

export default AuthReducer;